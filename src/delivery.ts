/*
### 1. Convertisseur de devises

- L'utilisateur doit pouvoir entrer le montant à convertir, la devise de départ et la devise de destination.
- Utilise un tableau de taux de conversion fixes pour les devises suivantes :
  - 1 EUR = 1.5 CAD = 130 JPY
  - 1 CAD = 0.67 EUR = 87 JPY
  - 1 JPY = 0.0077 EUR = 0.0115 CAD
- Retourne le montant converti dans la devise de destination.
*/
export function convertirDevise(
  montant: number,
  deviseEntree: 'EUR' | 'CAD' | 'JPY',
  deviseSortie: 'EUR' | 'CAD' | 'JPY'
): number {

  // TODO table de conversion
  if (deviseEntree === 'EUR') {
    if (deviseSortie === 'CAD') {
      return montant * 1.5;
    } else if (deviseSortie === 'JPY') {
      return montant * 130;
    } else {
      throw new Error('deviseSortie est incorrecte ' + deviseSortie)
    }
  } else if (deviseEntree === 'CAD') {
    if (deviseSortie === 'EUR') {
      return montant * 0.67;
    } else if (deviseSortie === 'JPY') {
      return montant * 87;
    } else {
      throw new Error('deviseSortie est incorrecte ' + deviseSortie)
    }
  } else if (deviseEntree === 'JPY') {
    if (deviseSortie === 'EUR') {
      return montant * 0.0077;
    } else if (deviseSortie === 'CAD') {
      return montant * 0.0115;
    } else {
      throw new Error('deviseSortie est incorrecte ' + deviseSortie)
    }
  } else {
    throw new Error('deviseEntree est incorrecte ' + deviseEntree)
  }
}

/*
### 2. Calculateur de frais de livraison

- L'utilisateur doit pouvoir entrer le poids du colis (en kg), les dimensions (longueur, largeur, hauteur en cm) et le pays de destination (France, Canada, Japon).
- Utilise les tarifs de livraison suivants basés sur le poids du colis :
  - Jusqu'à 1 kg : 10 EUR, 15 CAD, 1000 JPY
  - Entre 1 et 3 kg : 20 EUR, 30 CAD, 2000 JPY
  - Plus de 3 kg : 30 EUR, 45 CAD, 3000 JPY
- Ajoute un supplément de 5 EUR, 7.5 CAD, 500 JPY pour les colis dont la somme des dimensions dépasse 150 cm.
- Retourne les frais de livraison dans la devise correspondant au pays de destination.

dimension: 'longueur' | 'largeur' | 'hauteur' X
longueur, largeur, hauteur V
dimension: number[] V
dimension: [number, number, number] V -> tuple
dimension: {
  longueur: number,
  largeur: number,
  hauteur: number
} V
*/
export function calculFraisLivraison(poids: number, longueur: number, largeur: number, hauteur: number, destination: 'France' | 'Canada' | 'Japon'): number {
  let fraisLivraison = 0;
  if (destination === 'France') {
    if (poids < 1) {
      fraisLivraison = 10;
    } else if (poids < 3) {
      fraisLivraison = 20;
    } else {
      fraisLivraison = 30;
    }
  } else if (destination === 'Canada') {
    if (poids < 1) {
      fraisLivraison = 15;
    } else if (poids < 3) {
      fraisLivraison = 30;
    } else {
      fraisLivraison = 45;
    }
  } else if (destination === 'Japon') {
    if (poids < 1) {
      fraisLivraison = 1000;
    } else if (poids < 3) {
      fraisLivraison = 2000;
    } else {
      fraisLivraison = 3000;
    }
  } else {
    throw new Error('Destination incorrecte ' + destination)
  }
  // Ajoute un supplément de 5 EUR, 7.5 CAD, 500 JPY pour les colis dont la somme des dimensions dépasse 150 cm.
  let supplement = calculSupplementLivraison(longueur, largeur, hauteur, destination);

  // TODO Retourne les frais de livraison dans la devise correspondant au pays de destination
  return fraisLivraison + supplement;
}

function calculSupplementLivraison(longueur: number, largeur: number, hauteur: number, destination: string) {
  let supplement = 0;
  if (longueur + largeur + hauteur > 150) {
    if (destination === 'France') {
      supplement = 5;
    } else if (destination === 'Canada') {
      supplement = 7.5;
    } else if (destination === 'Japon') {
      supplement = 500;
    } else {
      throw new Error('Destination incorrecte ' + destination)
    }
  }
  return supplement;
}

/*
### 3. Calcul de frais de douanes

- L'utilisateur doit pouvoir calculer les frais de douane basés sur la valeur déclarée du colis et le pays de destination (l'origine des colis sera toujours la France).
  - Pour le Canada, 15 % de taxes pour les envois de plus de 20 CAD
  - Pour le Japon, 10 % de taxes pour les envois de plus de 5000 JPY
  - Pas de taxes supplémentaires pour la France.
- Retourne les frais de douane dans la devise correspondant au pays de destination.
*/